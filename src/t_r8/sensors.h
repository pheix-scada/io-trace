#ifndef __sensors_h
#define __sensors_h

char *inputs[] = {
    "baget_plk_DI_1_1",
    "baget_plk_DI_1_2",
    "baget_plk_DI_1_3",
    "baget_plk_DI_1_4",
    "baget_plk_DI_1_5",
    "baget_plk_DI_1_6",
    "baget_plk_DI_1_7",
    "baget_plk_DI_1_8",
    "baget_plk_DI_2_1",
    "baget_plk_DI_2_2",
    "baget_plk_DI_2_3",
    "baget_plk_DI_2_4",
    "baget_plk_DI_2_5",
    "baget_plk_DI_2_6",
    "baget_plk_DI_2_7",
    "baget_plk_DI_2_8"
};

#ifndef USO
    char *outputs[] = {
        "baget_plk_DO_1_1",
        "baget_plk_DO_1_2",
        "baget_plk_DO_1_3",
        "baget_plk_DO_1_4",
        "baget_plk_DO_1_5",
        "baget_plk_DO_1_6",
        "baget_plk_DO_1_7",
        "baget_plk_DO_1_8",
        "baget_plk_DO_2_1",
        "baget_plk_DO_2_2",
        "baget_plk_DO_2_3",
        "baget_plk_DO_2_4",
        "baget_plk_DO_2_5",
        "baget_plk_DO_2_6",
        "baget_plk_DO_2_7",
        "baget_plk_DO_2_8"
    };
#else
    char *outputs[] = {
        "baget_uso_DO_1_1",
        "baget_uso_DO_1_2",
        "baget_uso_DO_1_3",
        "baget_uso_DO_1_4",
        "baget_uso_DO_1_5",
        "baget_uso_DO_1_6",
        "baget_uso_DO_1_7",
        "baget_uso_DO_1_8",
    };
#endif

#endif //__sensors_h
