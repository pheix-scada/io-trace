#include "mlcp.h"
#include "sensors.h"

// PLC
#ifndef USO
    #define CONTROLLER  52382
    #define OUTPUTS     16
#else
// USO
    #define CONTROLLER  52384
    #define OUTPUTS     8
#endif

int
main(int argc, char *argv[])
{
    int rc   = 1;
    Url *url = url_parse(URLPATHRD);

    printf(
        "libmlcp version: %d.%d.%d-%s\n",
        MLCP_VERSION_MAJOR,
        MLCP_VERSION_MINOR,
        MLCP_VERSION_RELEASE,
        MLCP_VERSION_BUILD
    );

    if (outputs != NULL) {
        struct mlcp_tags * mlcp_t =
            rest_mlcp_init_tags(CONTROLLER, outputs, OUTPUTS);
        if (mlcp_t != NULL) {
            rc = rest_mlcp_get_ctrl_tags(mlcp_t, SRVADDR, url);
            if (rc == 0 ) {
                if ( mlcp_t->status > -1) {
                    printf(
                        "***INF - %03d tags are fetched\n",
                        mlcp_t->len
                    );
                    rest_mlcp_print_tags(mlcp_t);
                }
                else {
                    fprintf(
                        stderr,
                        "***ERR: tags fetching failure\n"
                    );
                    return 1;
                }
            }
            else {
                fprintf(
                    stderr,
                    "***ERR: request failure\n"
                );
                return 1;
            }
            rest_mlcp_free_tags(mlcp_t);
        }
        else {
            fprintf(
                stderr,
                "***ERR: control params struct is NULL\n"
            );
            return 1;
        }
    }
    else {
        fprintf(
            stderr,
            "***ERR: names array is NULL\n"
        );
        return 1;
    }
    return 0;
}
