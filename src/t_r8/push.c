#include "mlcp.h"
#include "sensors.h"

#define CONTROLLER  52381
#define INPUTS      16

static int iosrv_push(void);
static int push_tags(double * tagvalues);
static void fill_tags_with_vals(struct mlcp_tags * mlcp_t, double * values);

int
main(int argc, char *argv[])
{
    return iosrv_push();
}

static int
iosrv_push(void)
{
    int i;

    printf(
        "libmlcp version: %d.%d.%d-%s\n",
        MLCP_VERSION_MAJOR,
        MLCP_VERSION_MINOR,
        MLCP_VERSION_RELEASE,
        MLCP_VERSION_BUILD
    );

    srand(time(0));

    double * newvalues = NULL;
    newvalues = calloc(INPUTS, sizeof(double));

    for(i=0; i<INPUTS; i++) {
        double rnd   = rand();
        newvalues[i] = ((rnd/100000) + 1);
    }

    if (newvalues != NULL) {
        if (push_tags(newvalues)) {
            free(newvalues);
            return 1;
        }
        free(newvalues);
    }
    return 0;
}

static int
push_tags(double * tagvalues)
{
    Url *url = url_parse(URLPATHWR);
    if (inputs != NULL) {
        struct mlcp_tags * mlcp_t =
            rest_mlcp_init_tags(CONTROLLER, inputs, INPUTS);
        if (mlcp_t != NULL) {
            fill_tags_with_vals(mlcp_t, tagvalues);
            int rc = rest_mlcp_set_ctrl_tags(mlcp_t, SRVADDR, url);
            if (rc == 0) {
                if (mlcp_t->status > -1) {
                    printf(
                        "***INF - %03d tags are pushed (status: %d)\n",
                        mlcp_t->len,
                        mlcp_t->status
                    );
                    rest_mlcp_print_tags(mlcp_t);
                }
                else {
                    fprintf(
                        stderr,
                        "***ERR: tags pushing failure\n"
                    );
                    return 1;
                }
            }
            else {
                fprintf(
                    stderr,
                    "***ERR: request failure\n"
                );
                return 1;
            }
            rest_mlcp_free_tags(mlcp_t);
        }
        else {
            fprintf(
                stderr,
                "***ERR: control params struct is NULL\n"
            );
            return 1;
        }
    }
    else {
        fprintf(
            stderr,
            "***ERR: names array is NULL\n"
        );
        return 1;
    }
    return 0;
}

static void
fill_tags_with_vals(struct mlcp_tags * mlcp_t, double * values)
{
    int i;

    if (mlcp_t != NULL) {
        for (i = 0; i<mlcp_t->len; i++) {
            if (mlcp_t->tags != NULL) {
                double val = 0;

                if (values != NULL && values[i] >= 0) {
                    val = values[i];
                }

                mlcp_t->tags[i].val = val;
            }
        }
    }

    return;
}
