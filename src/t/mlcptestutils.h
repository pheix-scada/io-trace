#ifndef __MLCPUTILS_H
#define __MLCPUTILS_H

#include <stdlib.h>
#include <stdio.h>

#include <dbg.h>
#include <request.h>

#include "sensors.h"

int get_sensors_num(void);
char * get_sensors_name(int index);
int get_first(void);
int get_length(int first_value);
char ** names_for_iter(int start, int length);
void free_names( char ** names, int length );
void fill_tags_with_random_vals(struct mlcp_tags * mlcp_t);

#endif // __MLCPUTILS_H
