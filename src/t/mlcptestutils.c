#include "mlcptestutils.h"

int
get_sensors_num(void) {
    return ARRAY_SIZE(sensors);
}

char *
get_sensors_name(int index) {
    int sensnum = get_sensors_num();
    if ( index > sensnum - 1 ) {
        return NULL;
    }
    return sensors[index];
}

int
get_first(void) {
    int sensnum = get_sensors_num();
    return ( rand() % sensnum) ;
}

int
get_length(int first_value) {
    int sensnum = get_sensors_num();
    int currsensors = sensnum - first_value;
    int last_value  = first_value + rand() % currsensors;
    if ( last_value > sensnum) { last_value = sensnum; }
    return ( last_value - first_value );
}

char **
names_for_iter(int start, int length)
{
    int i;
    char ** names = NULL;
    names = calloc( length, sizeof(char *) );
    if ( names != NULL ) {
        for( i=0; i<length; i++) {
            names[i] = calloc( TAG_LEN, sizeof(char) );
            if ( names[i] != NULL ) {
                char * ptr = strcpy(names[i],sensors[start+i]);
                if (ptr == NULL) {
                    fprintf(
                        stderr,"***ERR: could not store sensor name <%s>\n",
                        sensors[ start + i ]
                    );
                    return NULL;
                }
            }
            else {
                return NULL;
            }
        }
    }
    //printf("names are generated!\n");
    return names;
}

void free_names( char ** names, int length ) {
    int i;
    for( i=0; i<length; i++) {
        if ( names[i] != NULL ) {
            free(names[i]);
        }
    }
    if (names != NULL) {
        free(names);
    }
}

void
fill_tags_with_random_vals(struct mlcp_tags * mlcp_t)
{
    int i;
    srand(time(0));
    if ( mlcp_t != NULL ) {
        for ( i = 0; i<mlcp_t->len; i++ ) {
            if ( mlcp_t->tags != NULL ) {
                double rnd   = rand();
                mlcp_t->tags[i].val = ((rnd/100000) + 1);
            }
        }
    }
    return;
}
