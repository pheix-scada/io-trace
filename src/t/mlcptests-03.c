#include "mlcp.h"

#define CONTROLLER  574
int
main(int argc, char *argv[])
{
    int i;
    int rc      = 1;
    Url *url    = url_parse(URLPATHRD);

    printf(
        "libmlcp version: %d.%d.%d-%s\n",
        MLCP_VERSION_MAJOR,
        MLCP_VERSION_MINOR,
        MLCP_VERSION_RELEASE,
        MLCP_VERSION_BUILD
    );

    srand(time(0));
    for( i=0; i<TESTITERS; i++) {
        struct mlcp_tags * mlcp_t =
            rest_mlcp_init_tags( CONTROLLER, NULL, 0 );
        if ( mlcp_t != NULL ) {
            double random = rand();
            mlcp_t->command.com = i + 1;
            mlcp_t->command.val = random/1000 + 1;
            rc = rest_mlcp_command(mlcp_t, SRVADDR, url);
            if (rc == 0 ) {
                if ( mlcp_t->status > -1) {
                    printf(
                        "%08d: ***INF - com_id=%08d: %s\n",
                        i,
                        mlcp_t->command.com,
                        mlcp_t->message
                    );
                    //rest_mlcp_print_tags(mlcp_t);
                }
                else {
                    fprintf(
                        stderr,
                        "***ERR: tags fetching failure\n"
                    );
                    return 1;
                }
            }
            else {
                fprintf(
                    stderr,
                    "***ERR: request failure\n"
                );
                return 1;
            }
            rest_mlcp_free_tags(mlcp_t);
        }
        else {
            fprintf(
                stderr,
                "***ERR: control params struct is NULL\n"
            );
            return 1;
        }
    }
    return 0;
}
