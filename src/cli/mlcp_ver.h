#ifndef __MLCP_VER_H
#define __MLCP_VER_H

#define MLCP_VERSION_MAJOR   0
#define MLCP_VERSION_MINOR   0
#define MLCP_VERSION_RELEASE 86

#ifndef MLCP_VERSION_BUILD
    #define MLCP_VERSION_BUILD   "default-build-date"
#endif

#endif // __MLCP_VER_H
