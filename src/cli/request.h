#ifndef __REQUEST_H
#define __REQUEST_H

#include <url.h>
#include <rest_mlcp.h>

#define GETMETHOD    0
#define POSTMETHOD   1
#define READREQUEST  0
#define WRITEREQUEST 1
#define COMMREQUEST  2
#define HTTPDELIM    "\r\n\r\n"

int rest_mlcp_request(
    struct mlcp_tags * mlcp_t, char * server_addr, Url *url, int reqtype );

int rest_mlcp_get_ctrl_tags(
    struct mlcp_tags * mlcp_t, char * server_addr, Url *url );

int rest_mlcp_set_ctrl_tags(
    struct mlcp_tags * mlcp_t, char * server_addr, Url *url );

int rest_mlcp_command(
    struct mlcp_tags * mlcp_t, char * server_addr, Url *url );

#endif // __REQUEST_H
