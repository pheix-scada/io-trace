#include <stdlib.h>
#include <stdio.h>

#include <dbg.h>
#include <request.h>

#define CONTROLLER  2020

int
main(int argc, char *argv[])
{
    char server_addr[BUF_SIZE];
    int rc        = 1;
    Url *url      = NULL;
    char *tagnames[] = { "pressure_sensor", "temp_sensor", "impulse_counter", "ADC_01", "ADC_02", "ADC_03", "DI_100", "DI_299", "actuator" };
    struct mlcp_tags * mlcp_t = rest_mlcp_init_tags( CONTROLLER, tagnames, ARRAY_SIZE(tagnames) );

    if (argc != 3 ) {
        fprintf(stderr, "Usage(%d): cli <server_addr> <server_name>\n\n", argc);
        return 1;
    }

    if ( mlcp_t != NULL ) {
        if ( DEBUG_PRINTS ) {
            printf( "***INF: init params ok, len=%d\n", mlcp_t->len );
        }
    }
    else {
        fprintf(stderr, "***ERR: init mlcp tags error\n");
        return 1;
    }

    char * ptr = strcpy(server_addr,argv[1]);
    error_unless(ptr != NULL, "***ERR: could not store server_addr '%s'", argv[1]);

    url = url_parse(argv[2]);
    error_unless(url, "***ERR: invalid URL supplied: '%s'", argv[2]);

    rc = rest_mlcp_get_ctrl_tags(mlcp_t, server_addr, url);
    if (rc == 0 ) {
        if ( mlcp_t->status > -1) {
            printf("***INF: tags are successfully fetched\n");
            rest_mlcp_print_tags(mlcp_t);
        }
        else {
            printf("***ERR: tags fetching failure\n");
        }
    }
    else {
        printf("***ERR: request failure\n");
    }

    rest_mlcp_free_tags(mlcp_t);

    if (url) {
        url_free(url);
    }
    return rc;
error:
    if ( mlcp_t != NULL ) {
        rest_mlcp_free_tags(mlcp_t);
    }
    if (url) {
        url_free(url);
    }
    return 1;
}
