#include "polygon.h"

static Url *url_t = NULL;
static Url *url_f = NULL;
static struct mlcp_tags *mlcp_t = NULL;
static struct mlcp_tags *mlcp_f = NULL;
static double outvalues[MLCP_TAGS_TO_COUNT] = { 0.0 };
static double invalues[MLCP_TAGS_FROM_COUNT] = { 0.0 };

int
main (void)
{
    int i  = 0;
    int rc = 0;

    mlcp_init();

    srand(time(0));

    for (i = 0; i < MLCP_TAGS_TO_COUNT; i++) {
        int random_value = (rand() % (TAG_RAND_MAX - TAG_RAND_MIN + 1)) + TAG_RAND_MIN;

        mlcp_set_value(i, random_value);
    }

    if (!mlcp_push_values() && !mlcp_pull_values()) {
        printf("***INF: polygon test ok\n");
    }
    else {
        fprintf (stderr, "***ERR: polygon test is failed\n");

        rc = 1;
    }

    mlcp_free();

    return rc;
}

void
mlcp_set_value (int id, double value)
{
    if (value < 0) {
        printf("\tnegative value %05.1f for element %02d\n", value, id);
    }

    outvalues[id] = value;
}

double
mlcp_get_value (int id)
{
    return invalues[id];
}

void
fill_tags_with_vals (struct mlcp_tags *mlcp_t, double *values)
{
    int i;

    if (mlcp_t != NULL) {
        for (i = 0; i < mlcp_t->len; i++) {
            if (mlcp_t->tags != NULL) {
                mlcp_t->tags[i].val = values[i];
            }
        }
    }

    return;
}


int
mlcp_init ()
{
    url_t = url_parse (URLPATHWR);
    url_f = url_parse (URLPATHRD);
    mlcp_t = rest_mlcp_init_tags (CTRLID, tags_to_scada, MLCP_TAGS_TO_COUNT);
    mlcp_f =
        rest_mlcp_init_tags (CTRLID, tags_from_scada, MLCP_TAGS_FROM_COUNT);

    return 1;
}

int
mlcp_free ()
{
    url_free (url_t);
    url_free (url_f);
    rest_mlcp_free_tags (mlcp_t);
    rest_mlcp_free_tags (mlcp_f);

    url_t  = NULL;
    url_f  = NULL;
    mlcp_t = NULL;
    mlcp_f = NULL;

    return 1;
}

int
mlcp_push_values ()
{
    if (!url_t) {
        fprintf (stderr, "***ERR: url is null\n");

        return 1;
    }

    if (outvalues != NULL) {
        if (mlcp_t != NULL) {
            fill_tags_with_vals (mlcp_t, outvalues);
            int rc = rest_mlcp_set_ctrl_tags (mlcp_t, SRVADDR, url_t);

            if (rc == 0) {
                if (mlcp_t->status != -1) {
                    return 0;
                }
                else {
                    fprintf (stderr, "***ERR: tags pushing failure: %s\n", mlcp_t->message);
                }
            }
            else {
                fprintf (stderr, "***ERR: request failure\n");
            }
        }
        else {
            fprintf (stderr, "***ERR: control params struct is NULL\n");
        }
    }
    else {
        fprintf (stderr, "***ERR: names array is NULL\n");
    }

    return 1;
}


int
mlcp_pull_values ()
{
    int rc = 1;
    if (!url_f) {
        fprintf (stderr, "***ERR: url is null\n");

        return 0;
    }

    if (invalues != NULL) {
        if (mlcp_f != NULL) {
            rc = rest_mlcp_get_ctrl_tags (mlcp_f, SRVADDR, url_f);

            if (rc == 0) {
                if (mlcp_f->status > -1) {
                    int i = 0;

                    for (i = 0; i < MLCP_TAGS_FROM_COUNT; i++) {
                        invalues[i] = mlcp_f->tags[i].val;
                    }
                }
                else {
                    fprintf (stderr, "***ERR: tags fetching failure: %s\n", mlcp_t->message);

                    return 1;
                }
            }
            else {
                fprintf (stderr, "***ERR: request failure\n");

                return 1;
            }
        }
        else {
            fprintf (stderr, "***ERR: control params struct is NULL\n");

            return 1;
        }
    }

    return 0;
}
