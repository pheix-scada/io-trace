#ifndef __POLYGON_H
#define __POLYGON_H

#include "mlcp.h"
#include "sensors.h"

#define TAG_RAND_MIN -99
#define TAG_RAND_MAX  99

void mlcp_set_value (int id, double value);
double mlcp_get_value (int id);
void fill_tags_with_vals (struct mlcp_tags *mlcp_t, double *values);

int mlcp_init ();
int mlcp_free ();
int mlcp_push_values ();
int mlcp_pull_values ();

#endif //__POLYGON_H
