#include <rest_mlcp.h>

#include "sensors_azs.h"

char **
names_for_tanks(int names_l, int tanks_l, int sensors_l)
{
    int i;
    char ** tanknames = NULL;
    tanknames = calloc(names_l, sizeof(char *));
    if (tanknames != NULL) {
        for(i=0; i<names_l; i++) {
            tanknames[i] = calloc(TAG_LEN, sizeof(char));
            if (tanknames[i] != NULL) {
                int rc = 0;
                char postfix[TAG_LEN] = "\0";
                if (i<sensors_l*tanks_l) {
                    //char * ptr = strcpy(names[i],sensors[start+i]);
                    int tank = i / sensors_l;
                    if (tank > 0) {
                        sprintf(postfix, "_tank%d", tank);
                    }
                    rc = sprintf(tanknames[i], "%s%s", names[i % sensors_l], postfix);
                }
                else {
                    rc = sprintf(tanknames[i], "%s", ext_names[i-sensors_l*tanks_l]);
                }
                if (!(rc > 0)) {
                    fprintf(
                        stderr,"***ERR: could not store tag name <%s%s>\n",
                        names[i], postfix
                    );
                    return NULL;
                }
                else {
                    /*printf(
                        "***INF: tag name <%s> is generated\n",
                        tanknames[i]
                    );*/
                }

            }
            else {
                return NULL;
            }
        }
    }
    return tanknames;
}

void
free_names_for_tank(int length, char ** tanknames)
{
    int i;
    for( i=0; i<length; i++) {
        if (tanknames[i] != NULL) {
            free(tanknames[i]);
        }
    }
    if (tanknames != NULL) {
        free(tanknames);
    }
}
