#include "mlcp.h"

#define CONTROLLER  574
#define SENSORS     5

#ifndef TANKS
    #define TANKS   6
#endif

#ifndef EXTENDS
    #define EXTENDS 2
#endif

#define NAMES       (SENSORS*TANKS)+EXTENDS

double VALUES[TANKS][SENSORS];
double EXVALS[EXTENDS];

extern char ** names_for_tanks(int names_l, int tanks_l, int sensors_l);
extern void free_names_for_tank(int length, char ** tanknames);

int iosrv_push(void);
int push_tags(double * tagvalues);
void store_tag_values(int tank, double * values);
void fill_tags_with_vals(struct mlcp_tags * mlcp_t, double * values);

void store_tank_tag_values(int tank, double * values) {
    int i;
    if (tank < TANKS && tank >= 0) {
        for(i=0; i<SENSORS; i++) {
            VALUES[tank][i] = values[i];
        }
    }
}

void store_extend_tag_values(double * values) {
    int i;
    for(i=0; i<EXTENDS; i++) {
        EXVALS[i] = values[i];
    }
}

int
main(int argc, char *argv[])
{
    int i,j = 0;
    double * newvalues = NULL;
    double * extvalues = NULL;

    if (TANKS > 0) {
        for(i=1; i<=TANKS; i++) {
            newvalues = calloc(SENSORS, sizeof(double));
            if (newvalues != NULL) {
                for(j=0; j<SENSORS; j++) {
                    newvalues[j] = (double)(j+1)/(SENSORS*i);
                    /*printf("%f\n",newvalues[i]);*/
                }
                store_tank_tag_values(i-1, newvalues);
                free(newvalues);
            }
        }
    }

    if (EXTENDS > 0) {
        extvalues = calloc(SENSORS, sizeof(double));
        if (extvalues != NULL) {
            for(j=0; j<EXTENDS; j++) {
                extvalues[j] = j;
                /*printf("%f\n",newvalues[i]);*/
            }
            store_extend_tag_values(extvalues);
            free(extvalues);
        }
    }

    if (iosrv_push()) {
        return 1;
    }

    return 0;
}

int
iosrv_push(void)
{
    int i;

    printf(
        "libmlcp version: %d.%d.%d-%s\n",
        MLCP_VERSION_MAJOR,
        MLCP_VERSION_MINOR,
        MLCP_VERSION_RELEASE,
        MLCP_VERSION_BUILD
    );

    double * newvalues = NULL;
    newvalues = calloc(NAMES, sizeof(double));
    for(i=0; i<SENSORS*TANKS; i++) {
        int tank = i / SENSORS;
        newvalues[i] = VALUES[tank][i % SENSORS];
        //printf("%d: %f\n",i,newvalues[i]);
    }
    for(i=0; i<EXTENDS; i++) {
        newvalues[SENSORS*TANKS+i] = EXVALS[i];
        //printf("%d: %f\n",SENSORS*TANKS+i,newvalues[SENSORS*TANKS+i]);
    }
    if (newvalues != NULL) {
        if (push_tags(newvalues)) {
            free(newvalues);
            return 1;
        }
        free(newvalues);
    }
    return 0;
}

int
push_tags(double * tagvalues)
{
    Url *url = url_parse(URLPATHWR);
    char ** tanknames = names_for_tanks(NAMES, TANKS, SENSORS);
    if (tanknames != NULL) {
        struct mlcp_tags * mlcp_t =
            rest_mlcp_init_tags(CONTROLLER, tanknames, NAMES);
        if ( mlcp_t != NULL ) {
            fill_tags_with_vals(mlcp_t, tagvalues);
            int rc = rest_mlcp_set_ctrl_tags(mlcp_t, SRVADDR, url);
            if (rc == 0) {
                if ( mlcp_t->status > -1) {
                    printf(
                        "***INF - %03d tags are pushed (status: %d)\n",
                        mlcp_t->len,
                        mlcp_t->status
                    );
                    rest_mlcp_print_tags(mlcp_t);
                }
                else {
                    fprintf(
                        stderr,
                        "***ERR: tags pushing failure\n"
                    );
                    return 1;
                }
            }
            else {
                fprintf(
                    stderr,
                    "***ERR: request failure\n"
                );
                return 1;
            }
            rest_mlcp_free_tags(mlcp_t);
        }
        else {
            fprintf(
                stderr,
                "***ERR: control params struct is NULL\n"
            );
            return 1;
        }
        free_names_for_tank(NAMES, tanknames);
    }
    else {
        fprintf(
            stderr,
            "***ERR: names array is NULL\n"
        );
        return 1;
    }
    return 0;
}

void
fill_tags_with_vals(struct mlcp_tags * mlcp_t, double * values)
{
    int i;
    srand(time(0));
    if ( mlcp_t != NULL ) {
        for ( i = 0; i<mlcp_t->len; i++ ) {
            if ( mlcp_t->tags != NULL ) {
                double val = 0;
                if (values != NULL && values[i] >= 0) {
                    val = values[i];
                }
                mlcp_t->tags[i].val = val;
            }
        }
    }
    return;
}
