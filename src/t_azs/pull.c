#include "mlcp.h"

#define CONTROLLER  574
#define SENSORS     5

#ifndef TANKS
    #define TANKS   6
#endif

#ifndef EXTENDS
    #define EXTENDS 2
#endif

#define NAMES       (SENSORS*TANKS)+EXTENDS

extern char ** names_for_tanks(int names_l, int tanks_l, int sensors_l);
extern void free_names_for_tank(int length, char ** tanknames);

int
main(int argc, char *argv[])
{
    int rc   = 1;
    Url *url = url_parse(URLPATHRD);

    printf(
        "libmlcp version: %d.%d.%d-%s\n",
        MLCP_VERSION_MAJOR,
        MLCP_VERSION_MINOR,
        MLCP_VERSION_RELEASE,
        MLCP_VERSION_BUILD
    );

    char ** tanknames = names_for_tanks(NAMES, TANKS, SENSORS);
    if (tanknames != NULL) {
        struct mlcp_tags * mlcp_t =
            rest_mlcp_init_tags(CONTROLLER, tanknames, NAMES);
        if (mlcp_t != NULL) {
            rc = rest_mlcp_get_ctrl_tags(mlcp_t, SRVADDR, url);
            if (rc == 0 ) {
                if ( mlcp_t->status > -1) {
                    printf(
                        "***INF - %03d tags are fetched\n",
                        mlcp_t->len
                    );
                    rest_mlcp_print_tags(mlcp_t);
                }
                else {
                    fprintf(
                        stderr,
                        "***ERR: tags fetching failure\n"
                    );
                    return 1;
                }
            }
            else {
                fprintf(
                    stderr,
                    "***ERR: request failure\n"
                );
                return 1;
            }
            rest_mlcp_free_tags(mlcp_t);
        }
        else {
            fprintf(
                stderr,
                "***ERR: control params struct is NULL\n"
            );
            return 1;
        }
        free_names_for_tank(NAMES, tanknames);
    }
    else {
        fprintf(
            stderr,
            "***ERR: names array is NULL\n"
        );
        return 1;
    }
    return 0;
}
