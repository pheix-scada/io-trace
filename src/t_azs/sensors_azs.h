#ifndef __SENSORS_AZS_H
#define __SENSORS_AZS_H

char *names[] = {
    "level",
    "volume",
    "water",
    "temperature",
    "density",
};

char *ext_names[] = {
    "ready_led",
    "error_led",
};

double values[] = {
    1.101,
    2.202,
    3.303,
    4.404,
    5.505
};

#endif //__SENSORS_AZS_H
