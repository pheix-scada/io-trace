# Usage
# =====
#
# 1. Compiling for POSIX platforms OCPB, Linux, etc...
# $ make
# or
# $ make PLATFORM=posix
#
# 2. Compiling for Win32
# $ make PLATFORM=win32
#
# HINT: Default platform is POSIX

PLATFORM := posix

ARCH=x64
PREFIX=
ECFLGS=$(ENV_CFLAGS)
CFLAGS=-static -Wall -Werror -g -I./src/cli -I./src/cli/uri/include/uriparser -I./src/cli/uri/include -I./src/cli/json/include -D__linux__ $(ECFLGS)
DATE=$(shell date +%FT%T%Z)

ifeq ($(PLATFORM),win32)
	LDFLAGS=-DMLCP_VERSION_BUILD=\"$(DATE)\" -lpthread -lws2_32
else
	LDFLAGS=-DMLCP_VERSION_BUILD=\"$(DATE)\" -lpthread
endif

OUTPUTPATH=debug
OUTPUTLIBPATH=$(OUTPUTPATH)/lib
OUTPUTHEADERS=$(OUTPUTLIBPATH)/include
SOURCES=$(wildcard src/cli/*.c src/cli/json/src/*.c src/cli/uri/src/*.c)
TESTSOURCES=$(wildcard src/t/*.c)
TESTAZSSOURCES=$(wildcard src/t_azs/*.c)
TESTR8SOURCES=$(wildcard src/t_r8/*.c)
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))
LIBSOURCES=$(filter-out src/cli/cli.c,$(SOURCES))
LIBOBJECTS=$(patsubst %.c,%.o,$(LIBSOURCES))

TEST1SOURCES=$(filter-out src/t/mlcptests-02.c src/t/mlcptests-03.c src/t/mlcptests-04.c,$(TESTSOURCES))
TEST1OBJECTS=$(patsubst %.c,%.o,$(TEST1SOURCES))
TEST2SOURCES=$(filter-out src/t/mlcptests-01.c src/t/mlcptests-03.c src/t/mlcptests-04.c,$(TESTSOURCES))
TEST2OBJECTS=$(patsubst %.c,%.o,$(TEST2SOURCES))
TEST3SOURCES=$(filter-out src/t/mlcptests-01.c src/t/mlcptests-02.c src/t/mlcptests-04.c,$(TESTSOURCES))
TEST3OBJECTS=$(patsubst %.c,%.o,$(TEST3SOURCES))
TEST4SOURCES=$(filter-out src/t/mlcptests-01.c src/t/mlcptests-02.c src/t/mlcptests-03.c,$(TESTSOURCES))
TEST4OBJECTS=$(patsubst %.c,%.o,$(TEST4SOURCES))

TESTAZSPULLSOURCES=$(filter-out src/t_azs/push.c,$(TESTAZSSOURCES))
TESTAZSPULLOBJECTS=$(patsubst %.c,%.o,$(TESTAZSPULLSOURCES))
TESTAZSPUSHSOURCES=$(filter-out src/t_azs/pull.c,$(TESTAZSSOURCES))
TESTAZSPUSHOBJECTS=$(patsubst %.c,%.o,$(TESTAZSPUSHSOURCES))

TESTR8PULLSOURCES=$(filter-out src/t_r8/push.c,$(TESTR8SOURCES))
TESTR8PULLOBJECTS=$(patsubst %.c,%.o,$(TESTR8PULLSOURCES))
TESTR8PUSHSOURCES=$(filter-out src/t_r8/pull.c,$(TESTR8SOURCES))
TESTR8PUSHOBJECTS=$(patsubst %.c,%.o,$(TESTR8PUSHSOURCES))

TESTPOLYGONSOURCES=$(wildcard src/t_polygon/*.c)
TESTR8PUSHOBJECTS=$(patsubst %.c,%.o,$(TESTPOLYGONSOURCES))

LIBNAME=$(OUTPUTLIBPATH)/libmlcp-$(ARCH).a
TARGET=debug/cli
TEST1=debug/test1
TEST2=debug/test2
TEST3=debug/test3
TEST4=debug/test4
TESTAZSPULL=debug/pull
TESTAZSPUSH=debug/push
TESTR8PULL=debug/pull_r8
TESTR8PUSH=debug/push_r8
TESTPOLYGON=debug/polygon

all: $(TARGET)
test1: $(TEST1)
test2: $(TEST2)
test3: $(TEST3)
test4: $(TEST4)
testazspull: $(TESTAZSPULL)
testazspush: $(TESTAZSPUSH)
testr8pull: $(TESTR8PULL)
testr8push: $(TESTR8PUSH)
testpolygon: $(TESTPOLYGON)

testazspullsimple:
	@ EXTDEFINES="-DTANKS=1 -DEXTENDS=0" make testazspull
testazspullextend:
	@ EXTDEFINES="-DTANKS=6 -DEXTENDS=2" make testazspull

testazspushsimple:
	@ EXTDEFINES="-DTANKS=1 -DEXTENDS=0" make testazspush
testazspushextend:
	@ EXTDEFINES="-DTANKS=6 -DEXTENDS=2" make testazspush

test: clean lib test1 test2 test3 test4
testazssimple: clean lib testazspullsimple testazspushsimple
testazsextend: clean lib testazspullextend testazspushextend
testr8:	clean lib testr8pull testr8push
testpol: clean lib testpolygon

$(TARGET): build $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)

$(TEST1): build $(TEST1SOURCES)
	$(CC) -Wall -Werror -g -I./src/t -I$(OUTPUTHEADERS) $(TEST1SOURCES) -L$(OUTPUTLIBPATH) $(ECFLGS) -lmlcp-x64 $(LDFLAGS) -o $@

$(TEST2): build $(TEST2SOURCES)
	$(CC) -Wall -Werror -g -I./src/t -I$(OUTPUTHEADERS) $(TEST2SOURCES) -L$(OUTPUTLIBPATH) $(ECFLGS) -lmlcp-x64 $(LDFLAGS) -o $@

$(TEST3): build $(TEST3SOURCES)
	$(CC) -Wall -Werror -g -I./src/t -I$(OUTPUTHEADERS) $(TEST3SOURCES) -L$(OUTPUTLIBPATH) $(ECFLGS) -lmlcp-x64 $(LDFLAGS) -o $@

$(TEST4): build $(TEST4SOURCES)
	$(CC) -Wall -Werror -g -I./src/t -I$(OUTPUTHEADERS) $(TEST4SOURCES) -L$(OUTPUTLIBPATH) $(ECFLGS) -lmlcp-x64 $(LDFLAGS) -o $@

$(TESTAZSPULL): build $(TESTAZSPULLSOURCES)
	$(CC) -Wall -Werror -g -I./src/t $(EXTDEFINES) -I$(OUTPUTHEADERS) $(TESTAZSPULLSOURCES) $(ECFLGS) -L$(OUTPUTLIBPATH) -lmlcp-x64 $(LDFLAGS) -o $@

$(TESTAZSPUSH): build $(TESTAZSPUSHSOURCES)
	$(CC) -Wall -Werror -g -I./src/t $(EXTDEFINES) -I$(OUTPUTHEADERS) $(TESTAZSPUSHSOURCES) $(ECFLGS) -L$(OUTPUTLIBPATH) -lmlcp-x64 $(LDFLAGS) -o $@

$(TESTR8PULL): build $(TESTR8PULLSOURCES)
	$(CC) -Wall -Werror -g -I./src/t $(EXTDEFINES) -I$(OUTPUTHEADERS) $(TESTR8PULLSOURCES) $(ECFLGS) -L$(OUTPUTLIBPATH) -lmlcp-x64 $(LDFLAGS) -o $@

$(TESTR8PUSH): build $(TESTR8PUSHSOURCES)
	$(CC) -Wall -Werror -g -I./src/t $(EXTDEFINES) -I$(OUTPUTHEADERS) $(TESTR8PUSHSOURCES) $(ECFLGS) -L$(OUTPUTLIBPATH) -lmlcp-x64 $(LDFLAGS) -o $@

$(TESTPOLYGON): build $(TESTPOLYGONSOURCES)
	$(CC) -Wall -Werror -g -I./src/t $(EXTDEFINES) -I$(OUTPUTHEADERS) $(TESTPOLYGONSOURCES) $(ECFLGS) -L$(OUTPUTLIBPATH) -lmlcp-x64 $(LDFLAGS) -o $@

build:
	@mkdir -p $(OUTPUTPATH)

dev: CFLAGS+=-DNDEBUG
dev: all

clean:
	rm -rf sources.txt ./debug ./src/t/*.o ./src/cli/*.o ./src/cli/uri/src/*.o ./src/cli/json/src/*.o *.dSYM

lib: $(LIBOBJECTS)
	@ mkdir -p $(OUTPUTHEADERS)
	@ cp -f src/cli/*.h $(OUTPUTHEADERS)
	@ cp -f src/cli/uri/include/uriparser/*.h $(OUTPUTHEADERS)
	@ cp -f src/cli/json/include/*.h $(OUTPUTHEADERS)
	@ sed -i "s/default-build-date/$(DATE)/g" $(OUTPUTHEADERS)/mlcp_ver.h
	@ $(PREFIX)ar -r $(LIBNAME) $?
