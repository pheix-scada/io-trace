#!/bin/bash

DOCPATH=../sources.txt
SRCPATH=../src/
DELIM=--------------------------------
SOURCES=()
while IFS=  read -r -d $'\0'; do
    SOURCES+=("$REPLY")
done < <(find ${SRCPATH} -type f -print0)

if [ -f "${DOCPATH}" ]; then
    rm -f ${DOCPATH}
fi

for FILE in "${SOURCES[@]}"
do
   :
   HEADER=`echo ${FILE} | sed 's/..\/src\///'`
   printf "${HEADER}\n%.*s\n\n" ${#HEADER} ${DELIM} >> ${DOCPATH}
   cat ${FILE} >> ${DOCPATH}
   printf "\n\n" >> ${DOCPATH}
done

echo Doc from sources is generated!
