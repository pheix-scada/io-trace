#!/usr/bin/env bash

WORKPATH=$1
if [ -z "${WORKPATH}" ]; then
    echo Set current dir as default path prefix!
    WORKPATH=.
else
    echo Use user defined workpath ${WORKPATH}
fi

PUSHCMD=`${WORKPATH}/debug/push | grep -P "\t" | sed "s/\t[0-9]*\:\s//" | sed "s/=>\s/=> \"/" | sed ':a;N;$!ba;s/\n/",\n/g'`
if [ -z "${PUSHCMD}" ]; then
    echo Push failed!
    exit 1;
fi
PULLCMD=`${WORKPATH}/debug/pull | grep -P "\t" | sed "s/\t[0-9]*\:\s//" | sed "s/=>\s/=> \"/" | sed ':a;N;$!ba;s/\n/",\n/g'`
if [ -z "${PULLCMD}" ]; then
    echo Pull failed!
    exit 1;
fi

PUSH="my %push = ( "${PUSHCMD}"\");"
PULL="my %pull = ( "${PULLCMD}"\");"

perl -MTest::More -E "$PUSH $PULL plan tests => 1; is_deeply(\%push,\%pull,'push equals pull'); done_testing();"
