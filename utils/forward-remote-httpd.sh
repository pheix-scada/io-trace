#!/bin/sh

PORTI=80
PORTO=8888
IPADI=127.0.0.100
IPADO=172.16.5.205

if [ ! -z $1 ] && [ ! -z $2 ] && [ ! -z $3 ]; then
    PORTI=$1
    IPADO=$2
    PORTO=$3
    echo "Use user-defined addr and ports: forward from ${PORTI} to ${IPADO}:${PORTO}"
else
    echo "Use default ports: forward from ${PORTI} to ${IPADO}:${PORTO}"
fi

socat TCP-LISTEN:${PORTI},reuseaddr,fork TCP:${IPADO}:${PORTO}
